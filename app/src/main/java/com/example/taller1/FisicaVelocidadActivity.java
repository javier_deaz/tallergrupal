package com.example.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class FisicaVelocidadActivity extends AppCompatActivity implements View.OnClickListener {
    EditText distancia, tiempo;
    TextView resultado;
    Button calcular, regresarFV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fisica_velocidad);

        distancia = (EditText) findViewById(R.id.edtDistancia);
        tiempo = (EditText) findViewById(R.id.edtTiempo);
        resultado = (TextView) findViewById(R.id.tvResultados);
        calcular= (Button) findViewById(R.id.btnCalcular);
        regresarFV = (Button) findViewById(R.id.btnRegresar1);

        calcular.setOnClickListener(this);
        regresarFV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.btnCalcular:
                boolean validarCampos = camposCompletos();
                if(validarCampos){
                    calcularVelocidad();
                }else{
                    Toast.makeText(this, "ingrese todos los campo", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnRegresar1:
                i = new Intent(getApplicationContext(),HomeFisicaActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }

    public boolean camposCompletos(){
        String distancia_String = distancia.getText().toString();
        String tiempo_String = tiempo.getText().toString();

        if(TextUtils.isEmpty(distancia_String)){
            return false;
        }else if(TextUtils.isEmpty(tiempo_String)){
            return false;
        }else{
            return true;
        }
    }

    public void calcularVelocidad(){
        Float distancia_float = Float.parseFloat(distancia.getText().toString());
        Float tiempo_float = Float.parseFloat(tiempo.getText().toString());

        if(tiempo_float != 0){
            float resul = distancia_float / tiempo_float;
            resultado.setText("La velocidad es: " + String.valueOf(resul) + " m/s");
        }else{
            Toast.makeText(this, "Ingrese valor diferente de cero", Toast.LENGTH_SHORT).show();
        }

    }
}