package com.example.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeFisicaActivity extends AppCompatActivity implements View.OnClickListener {

    Button regresarF, velocidad, fuerza, voltaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_fisica);
        velocidad = (Button) findViewById(R.id.btnVelocidad);
        fuerza = (Button) findViewById(R.id.btnFuerza);
        voltaje = (Button) findViewById(R.id.btnVoltaje);
        regresarF = (Button) findViewById(R.id.btnFRegresar);

        velocidad.setOnClickListener(this);
        fuerza.setOnClickListener(this);
        voltaje.setOnClickListener(this);
        regresarF.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.btnVelocidad:
                i = new Intent(getApplicationContext(),FisicaVelocidadActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.btnFuerza:
                i = new Intent(getApplicationContext(),FisicaFuerzaActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.btnVoltaje:
                i = new Intent(getApplicationContext(),FisicaVoltajeActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.btnFRegresar:
                i = new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }
}