package com.example.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FisicaFuerzaActivity extends AppCompatActivity implements View.OnClickListener {
    EditText masa, aceleracion;
    TextView resultado;
    Button calcularF, regresarFF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fisica_fuerza);
        masa = (EditText) findViewById(R.id.edtMasa);
        aceleracion = (EditText) findViewById(R.id.edtAceleracion);
        resultado = (TextView) findViewById(R.id.tvResultadoF);
        calcularF = (Button) findViewById(R.id.btnCalcularF);
        regresarFF = (Button) findViewById(R.id.btnRegresarF);

        calcularF.setOnClickListener(this);
        regresarFF.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.btnCalcularF:
                boolean validarCampos = camposCompletos();
                if(validarCampos){
                    calcularFuerza();
                }else{
                    Toast.makeText(this, "ingrese todos los campo", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnRegresarF:
                i = new Intent(getApplicationContext(),HomeFisicaActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }

    public boolean camposCompletos(){
        String masa_String = masa.getText().toString();
        String aceleracion_String = aceleracion.getText().toString();

        if(TextUtils.isEmpty(masa_String)){
            return false;
        }else if(TextUtils.isEmpty(aceleracion_String)){
            return false;
        }else{
            return true;
        }
    }

    public void calcularFuerza(){
        Float masa_float = Float.parseFloat(masa.getText().toString());
        Float ace_float = Float.parseFloat(aceleracion.getText().toString());

        float result = masa_float * ace_float;
        resultado.setText("La fuerza es: " + String.valueOf(result) + " N");
    }

}